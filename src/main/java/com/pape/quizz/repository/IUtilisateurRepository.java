package com.pape.quizz.repository;

import com.pape.quizz.entity.UtilisateurEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUtilisateurRepository extends JpaRepository<UtilisateurEntity, Integer> {
}
