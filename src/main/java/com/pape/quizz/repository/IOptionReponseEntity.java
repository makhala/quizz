package com.pape.quizz.repository;

import com.pape.quizz.entity.OptionReponseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IOptionReponseEntity extends JpaRepository<OptionReponseEntity,Integer> {
}
