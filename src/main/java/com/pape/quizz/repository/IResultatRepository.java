package com.pape.quizz.repository;

import com.pape.quizz.entity.ResultatEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IResultatRepository extends JpaRepository<ResultatEntity, Integer> {
}
