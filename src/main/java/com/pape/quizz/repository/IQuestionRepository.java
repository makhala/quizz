package com.pape.quizz.repository;

import com.pape.quizz.entity.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IQuestionRepository extends JpaRepository<QuestionEntity, Integer> {
}
