package com.pape.quizz.repository;

import com.pape.quizz.entity.QuizEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IQuizzRepository extends JpaRepository<QuizEntity,Integer> {
}
