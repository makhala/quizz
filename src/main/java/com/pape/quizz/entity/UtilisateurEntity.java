package com.pape.quizz.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the utilisateur database table.
 */
@Entity
@Table(name = "utilisateur")
@NamedQuery(name = "Utilisateur.findAll", query = "SELECT u FROM UtilisateurEntity u")
@Getter
@Setter
@NoArgsConstructor
public class UtilisateurEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private int userID;

    @Column(length = 255)
    private String avatar;

    @Column(nullable = false, length = 255)
    private String email;

    @Column(nullable = false, length = 255)
    private String password;

    @Column(nullable = false, length = 255)
    private String username;

    //bi-directional many-to-one association to Quiz
    @OneToMany(mappedBy = "utilisateur", fetch = FetchType.EAGER)
    private List<QuizEntity> quizEntities;

    //bi-directional many-to-one association to Resultatquiz
    @OneToMany(mappedBy = "utilisateur", fetch = FetchType.EAGER)
    private List<ResultatEntity> resultatEntityQuizs;


    public QuizEntity addQuiz(QuizEntity quizEntity) {
        getQuizEntities().add(quizEntity);
        quizEntity.setUtilisateurEntity(this);

        return quizEntity;
    }

    public QuizEntity removeQuiz(QuizEntity quizEntity) {
        getQuizEntities().remove(quizEntity);
        quizEntity.setUtilisateurEntity(null);

        return quizEntity;
    }


    public ResultatEntity addResultatQuiz(ResultatEntity resultatquiz) {
        getResultatEntityQuizs().add(resultatquiz);
        resultatquiz.setUtilisateurEntity(this);

        return resultatquiz;
    }

    public ResultatEntity removeResultatQuiz(ResultatEntity resultatquiz) {
        getResultatEntityQuizs().remove(resultatquiz);
        resultatquiz.setUtilisateurEntity(null);

        return resultatquiz;
    }

}