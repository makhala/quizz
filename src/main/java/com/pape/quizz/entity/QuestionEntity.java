package com.pape.quizz.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the question database table.
 */
@Entity
@Table(name = "question")
@NamedQuery(name = "Question.findAll", query = "SELECT q FROM QuestionEntity q")
@Getter
@Setter
@NoArgsConstructor
public class QuestionEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private int questionID;

    @Lob
    private String enonce;

    @Column(length = 50)
    private String typeQuestion;

    //bi-directional many-to-one association to OptionReponseEntity
    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
    private List<OptionReponseEntity> optionReponses;

    //bi-directional many-to-one association to Quiz
    @ManyToOne
    @JoinColumn(name = "QuizID")
    private QuizEntity quizEntity;

    public OptionReponseEntity addOptionrepons(OptionReponseEntity optionrepons) {
        getOptionReponses().add(optionrepons);
        optionrepons.setQuestionEntity(this);

        return optionrepons;
    }

    public OptionReponseEntity removeOptionrepons(OptionReponseEntity optionrepons) {
        getOptionReponses().remove(optionrepons);
        optionrepons.setQuestionEntity(null);

        return optionrepons;
    }
}