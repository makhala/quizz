package com.pape.quizz.entity;

import java.io.Serializable;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the quiz database table.
 * 
 */
@Entity
@Table(name="quiz")
@NamedQuery(name="Quiz.findAll", query="SELECT q FROM QuizEntity q")
@Getter
@Setter
@NoArgsConstructor
public class QuizEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int quizID;

	@Temporal(TemporalType.DATE)
	private Date dateCreation;

	@Lob
	private String description;

	@Column(nullable=false)
	private String titre;

	//bi-directional many-to-one association to Question
	@OneToMany(mappedBy="quiz", fetch=FetchType.EAGER)
	private List<QuestionEntity> questionEntities;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name="CreateurID")
	private UtilisateurEntity utilisateurEntity;

	//bi-directional many-to-one association to Resultatquiz
	@OneToMany(mappedBy="quiz", fetch=FetchType.EAGER)
	private List<ResultatEntity> resultatEntityQuizs;


	public QuestionEntity addQuestion(QuestionEntity questionEntity) {
		getQuestionEntities().add(questionEntity);
		questionEntity.setQuizEntity(this);

		return questionEntity;
	}

	public QuestionEntity removeQuestion(QuestionEntity questionEntity) {
		getQuestionEntities().remove(questionEntity);
		questionEntity.setQuizEntity(null);

		return questionEntity;
	}

	public ResultatEntity addResultatquiz(ResultatEntity resultatquiz) {
		getResultatEntityQuizs().add(resultatquiz);
		resultatquiz.setQuizEntity(this);

		return resultatquiz;
	}

	public ResultatEntity removeResultatquiz(ResultatEntity resultatquiz) {
		getResultatEntityQuizs().remove(resultatquiz);
		resultatquiz.setQuizEntity(null);

		return resultatquiz;
	}

}