package com.pape.quizz.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


/**
 * The persistent class for the optionreponse database table.
 */
@Entity
@Table(name = "optionreponse")
@NamedQuery(name = "OptionReponseEntity.findAll", query = "SELECT o FROM OptionReponseEntity o")
@Getter
@Setter
@NoArgsConstructor
public class OptionReponseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private int optionID;

    private byte estCorrect;

    @Lob
    private String texteOption;

    //bi-directional many-to-one association to Question
    @ManyToOne
    @JoinColumn(name = "QuestionID")
    private QuestionEntity questionEntity;

}