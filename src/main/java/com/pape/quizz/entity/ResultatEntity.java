package com.pape.quizz.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the resultatquiz database table.
 */
@Entity
@Table(name = "resultatquiz")
@NamedQuery(name = "Resultatquiz.findAll", query = "SELECT r FROM ResultatEntity r")
@Getter
@Setter
@NoArgsConstructor
public class ResultatEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private int resultID;

    @Temporal(TemporalType.DATE)
    private Date dateRealisation;

    private int score;

    //bi-directional many-to-one association to Utilisateur
    @ManyToOne
    @JoinColumn(name = "UserID")
    private UtilisateurEntity utilisateurEntity;

    //bi-directional many-to-one association to Quiz
    @ManyToOne
    @JoinColumn(name = "QuizID")
    private QuizEntity quizEntity;

}