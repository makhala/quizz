package com.pape.quizz.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UtilisateurDto {
    private int userID;
    private String avatar;
    private String email;
    private String password;
    private String username;
    private List<ResultatDto> resultatDtos;
}
