package com.pape.quizz.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class QuizzDto {
    private int quizID;
    private Date dateCreation;
    private String description;
    private String titre;
    private List<QuestionDto> questionDtos;
    private UtilisateurDto utilisateurDto;
    private List<ResultatDto> resultatDtos;
}
