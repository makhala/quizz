Drop database db_quizz;
create database db_quizz;
use db_quizz;
DROP TABLE IF EXISTS Utilisateur;

DROP TABLE IF EXISTS Quizz;

DROP TABLE IF EXISTS Question;

DROP TABLE IF EXISTS ResultatQuizz;

DROP TABLE IF EXISTS OptionReponse;

CREATE TABLE Utilisateur (
                             UserID INT auto_increment Primary KEY,
                             Username VARCHAR(255) UNIQUE NOT NULL,
                             Password VARCHAR(255) NOT NULL,
                             Email VARCHAR(255) UNIQUE NOT NULL,
                             Avatar VARCHAR(255)
);

CREATE TABLE Quizz (
                       QuizzID INT auto_increment PRIMARY KEY,
                       Titre VARCHAR(255) NOT NULL,
                       Description TEXT,
                       CreateurID INT Not null,
                       DateCreation DATE,
                       FOREIGN KEY (CreateurID) REFERENCES Utilisateur(UserID)
);

CREATE TABLE Question (
                          QuestionID INT auto_increment PRIMARY KEY,
                          QuizzID INT Not null,
                          Enonce TEXT,
                          TypeQuestion VARCHAR(50),
                          FOREIGN KEY (QuizzID) REFERENCES Quizz(QuizzID)
);

CREATE TABLE OptionReponse (
                               OptionID INT auto_increment PRIMARY KEY,
                               QuestionID INT Not null,
                               TexteOption TEXT,
                               EstCorrect boolean,
                               FOREIGN KEY (QuestionID) REFERENCES Question(QuestionID)
);

CREATE TABLE ResultatQuizz (
                               ResultID INT auto_increment PRIMARY KEY,
                               UserID INT Not null,
                               QuizzID INT Not null,
                               Score INT,
                               DateRealisation DATE,
                               FOREIGN KEY (UserID) REFERENCES Utilisateur(UserID),
                               FOREIGN KEY (QuizzID) REFERENCES Quizz(QuizzID)
);